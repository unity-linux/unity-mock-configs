# mock group id allocate for Unity
%global mockgid 135

Name:		unity-mock-configs
Version:	30.01
Release:	1%{?dist}
Summary:	Mock core config files basic chroots

License:	GPLv2+
URL:		https://gitlab.com/unity-linux/unity-mock-configs
# Source is created by
# git clone https://gitlab.com/unity-linux/unity-mock-configs.git
# cd mock/mock-core-configs
# git reset --hard %{name}-%{version}
# tito build --tgz
Source:		%{name}-%{version}.tar.xz
BuildArch:	noarch

Requires:	mock-core-configs
Requires(pre):	shadow-utils
Requires(post): coreutils
# to detect correct default.cfg
Requires(post):	python3-dnf
Requires(post):	python3-hawkey
Requires(post):	system-release
Requires(post):	python3
Requires(post):	sed

%description
Config files which allow you to create chroots for:
 * Unity-Linux

%prep
%setup -q


%build
# nothing to do here


%install
mkdir -p %{buildroot}/%{_datadir}/distribution-gpg-keys/unitylinux
mkdir -p %{buildroot}%{_sysconfdir}/mock

cp -a *-primary %{buildroot}/usr/share/distribution-gpg-keys/unitylinux/
cp -a *.cfg %{buildroot}%{_sysconfdir}/mock/

%pre
# check for existence of mock group, create it if not found
getent group mock > /dev/null || groupadd -f -g %mockgid -r mock
exit 0

%post
ver=$(source /etc/os-release && echo $VERSION_ID | cut -d. -f1 | grep -o '[0-9]\+')
mock_arch=$(python3 -c "import dnf.rpm; import hawkey; print(dnf.rpm.basearch(hawkey.detect_arch()))")
cfg=unitylinux-$ver-${mock_arch}.cfg
if [ -e %{_sysconfdir}/mock/$cfg ]; then
    if [ "$(readlink %{_sysconfdir}/mock/default.cfg)" != "$cfg" ]; then
        ln -s $cfg %{_sysconfdir}/mock/default.cfg 2>/dev/null || ln -s -f $cfg %{_sysconfdir}/mock/default.cfg.rpmnew
    fi
else
    echo "Warning: file %{_sysconfdir}/mock/$cfg does not exist."
    echo "         unable to update %{_sysconfdir}/mock/default.cfg"
fi
:


%files
%license COPYING
%dir %{_sysconfdir}/mock
%{_sysconfdir}/mock/*.cfg
%{_datadir}/distribution-gpg-keys/unitylinux/*-primary

%changelog
